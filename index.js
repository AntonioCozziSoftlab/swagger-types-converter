var fs = require('fs');
var https = require('https');
var axios = require('axios');
var CodeGen = require('swagger-js-codegen').CodeGen;

const instance = axios.create({
    httpsAgent: new https.Agent({
        rejectUnauthorized: false,
    }),
});

const apiUrl = process.argv[2]

instance
    .get(apiUrl)
    .then(response => {
        const json = response.data
        var tsSourceCode = CodeGen.getTypescriptCode({ swagger: json });
        fs.writeFile("api.types.ts", tsSourceCode, function (err) {
            if (err) return console.log(err);
            console.log('Types succesfully converted > api.types.ts');
        });
    })